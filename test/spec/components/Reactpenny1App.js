'use strict';

describe('Main', function () {
  var React = require('react/addons');
  var Reactpenny1App, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    Reactpenny1App = require('../../../src/scripts/components/Layout.js');
    component = React.createElement(Reactpenny1App);
  });

  it('should create a new instance of Reactpenny1App', function () {
    expect(component).toBeDefined();
  });
});
