/*
 * Webpack development server configuration
 *
 * This file is set up for serving the webpak-dev-server, which will watch for changes and recompile as required if
 * the subfolder /webpack-dev-server/ is visited. Visiting the root will not automatically reload.
 */
'use strict';
var webpack = require('webpack');
var webpackPostcssTools = require('webpack-postcss-tools');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var atcss = require('atcss');

var map = webpackPostcssTools.makeVarMap('src/styles/app.css');


module.exports = {
    output: {
        filename: 'app.js',
        publicPath: '/assets/',
        path: __dirname + '/assets'
    },

    cache: true,
    debug: true,
    devtool: false,
    entry: [
        'webpack/hot/only-dev-server',
        './src/scripts/app.js'
    ],

    stats: {
        colors: true,
        reasons: true
    },

    resolve: {
       packageMains: [
            'webpack',
            'browser',
            'web',
            'browserify',
            ['jam', 'main'],
            'style',
            'main'
        ],
        extensions: ['', '.js'],
        modulesDirectories: ['node_modules', 'bower_components', 'web_modules'],
        alias: {
            jquery: "jquery-ajax.min.js"
        }
    },
    module: {
        preLoaders: [{
            test: '\\.js$',
            exclude: 'node_modules',
            loader: 'jshint'
        }],
        loaders: [{
            test: /\.js$/,
            loader: 'react-hot!jsx-loader?harmony'
        }, {
            test: /\.styl/,
            loader: 'style-loader!css-loader!stylus-loader'
        }, {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract("style",
                "css?importLoaders=1!postcss"
            )
            //loader: 'style!css?importLoaders=1!postcss'
        }, {
            test: /\.(png|jpg)$/,
            loader: 'url-loader?limit=8192'
        }, {
            test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: "url-loader?limit=10000&minetype=application/font-woff"
        }, {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: "file-loader"
        }]
    },

    postcss: [
        webpackPostcssTools.prependTildesToImports,

        require('postcss-custom-properties')({
            variables: map.vars
        }),

        require('postcss-custom-media')({
            extensions: map.media
        }),

        require('postcss-calc')
    ],

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin("app.css", {allChunks: true})/*,
        new webpack.ProvidePlugin({
            $: './src/scripts/vendor/jquery-ajax.min.js'
        })*/
    ]

};
