var Reflux = require('reflux');
var API = require('../api/API');

var UserActions = Reflux.createActions({
    registerUser: {asyncResult: true},
    loginUser: {asyncResult: true},
    logoutUser: {asyncResult: false}
});

UserActions.registerUser.listen(function () {
    var self = this;
    API.registerUser().success(function (data) {
        self.completed(data);
    });

});

UserActions.loginUser.listen(function (username, password) {
    var self = this;
    API.loginUser(username, password).then(function onFulfilled(auth_token) {
        localStorage.setItem('auth_token', auth_token);
        self.completed(username, auth_token);
    }, function onRejected(){});

});

UserActions.logoutUser.listen(function () {
    localStorage.removeItem('auth_token');
});

module.exports = UserActions;
