var Reflux = require('reflux');
var API = require('../api/API');

var AuctionActions = Reflux.createActions({
    getAuctions: {asyncResult: true},
    getAuction: {asyncResult: true},
    bid: {asyncResult: true},
    subscribe: {asyncResult: true},
    unsubscribe: {asyncResult: true}
});

AuctionActions.getAuctions.listen(function () {
    var self = this;
    API.getAuctions().success(function (data) {
        self.completed(data);
    });

});

AuctionActions.getAuction.listen(function (id) {
    var self = this;
    API.getAuction(id).success(function (data) {
        self.completed(data);
    });

});

AuctionActions.bid.listen(function (id){
    var self = this;
    API.bidOnAuction(id);
    //TODO handle error
});

AuctionActions.subscribe.listen(function (id){
    var self = this;
    API.subscribeToAuctionUpdate(id, function (data) {
        self.completed(JSON.parse(data.body));
    });
    //TODO handle error
});

AuctionActions.unsubscribe.listen(function (id){
    var self = this;
    API.unsubscribeFromAuctionUpdate(id);
    //TODO handle error
});
module.exports = AuctionActions;
