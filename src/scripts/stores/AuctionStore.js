var Reflux = require('reflux');
var AuctionActions = require('../actions/AuctionActions');

var AuctionStore = Reflux.createStore({
    listenables: [AuctionActions],

    getInitialState: function(){
        return {auction: {
            item:{
                name:'',
                images:[]
            }
        },
        loaded: false};
    },

    onGetAuctionCompleted: function(payload){
        this.trigger({'auction':payload, loaded: true});
    },
/*
    onBidCompleted: function(payload){
        this.trigger({'auction':payload, loaded:true});
    },
*/
    onSubscribeCompleted: function(payload){
        this.trigger(payload);
    }



});

module.exports = AuctionStore;
