var Reflux = require('reflux');
var UserActions = require('../actions/UserActions');

var state = {
    auth_token: null,
    username: null

};

var UserStore = Reflux.createStore({
    listenables: [UserActions],

    getInitialState: function () {
        return state;
    },
    onLoginUserCompleted: function (username, auth_token) {
        state.username = username;
        state.auth_token = auth_token;
        this.trigger(state);
    },
    onLogoutUser: function () {
        state.username = null;
        state.auth_token = null;
        this.trigger();
    },
    isLoggedIn: function (){return localStorage.getItem('auth_token');}
});

module.exports = UserStore;
