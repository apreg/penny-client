var Reflux = require('reflux');
var AuctionActions = require('../actions/AuctionActions');

var AuctionsStore = Reflux.createStore({
    listenables: [AuctionActions],

    getInitialState: function(){
        return {
            auctions: [],
            loaded: false};
    },

    onGetAuctionsCompleted: function(payload){
        this.trigger({'auctions':payload,
        loaded: true});
    }

});

module.exports = AuctionsStore;
