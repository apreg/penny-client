var $ = require("jquery");
var sockjs = require("sockjs-client");
var Stomp = require('stompjs');
var Promise = require('native-promise-only');


function API() {
    this.API_ROOT_URL = 'http://localhost:9999/api';
    this.stompClient = this.connectToWebSocket();
}

API.prototype.connectToWebSocket = function () {
    var socket = new sockjs('http://localhost:9999/auc');
    var stompClient = Stomp.over(socket);
    var self = this;
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
    });
    return stompClient;
}

API.prototype.disconnectFromWebSocket = function () {
    if (this.stompClient != null) {
        this.stompClient.disconnect();
    }
    console.log("Disconnected");
}

API.prototype.getAuctions = function () {
    return $.ajax({
        url: this.API_ROOT_URL + '/auctions',
        type: 'GET',
        cache: true,
        dataType: 'json'
    });
};


API.prototype.getAuction = function (id) {
    return $.ajax({
        url: this.API_ROOT_URL + '/auctions/' + id,
        type: 'GET',
        cache: true,
        dataType: 'json'
    });
};

API.prototype.bidOnAuction = function (id) {
    this.stompClient.send('/api/auctions/' + id + '/bid');
};

API.prototype.subscribeToAuctionUpdate = function (id, messageHandler) {
    this.stompClient.subscribe('/auctions/' + id + '/update', messageHandler);
};

API.prototype.unsubscribeFromAuctionUpdate = function (id) {
    this.stompClient.unsubscribe('/auctions/' + id + '/update');
};

API.prototype.registerUser = function (userName, email, password, birthDate) {

};

API.prototype.loginUser = function (username, password) {
    var self = this;
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: self.API_ROOT_URL + '/login',
            type: 'POST',
            cache: false,
            //dataType: 'json',
            data: {'username': username, 'password': password},
            success: function (data, textStatus, jqXHR) {
                resolve(jqXHR.getResponseHeader('X-AUTH-TOKEN'));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reject(errorThrown);
            }
        });
    })

};

module.exports = new API();
