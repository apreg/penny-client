'use strict';

var React = require('react/addons');
var Router = require('react-router');
var Link = Router.Link;

var Route = Router.Route;

var AuctionInfo = React.createClass({
    render: function () {
        return (
            <div class="AuctionInfo">
                <p class="AuctionInfo-price">
                    <span className="price">$1.98</span>
                    <span className="AuctionInfo-price-currency">USD</span>
                </p>

                <p class="Auction">00:00:10</p>

                <h2 class="margin-five username-height">
                    <span>
                        <img src="https://static.quibids.com/site/images/avatards/87.png" width="64" height="64" class="user-icon winning_avatar" style="display: inline;">
                    </span>
                    <br>
                    <span class="winning">legendnick27</span>
                </h2>

                <p>
                    <a href="#" class="buttons bid large orange">Bid Now</a>
                </p>

                <ul className="PriceBreakdown">
                    <li>Value Price:
                        <span class="float-right" id="product_valueprice">$123.99</span>
                    </li>

                    <li>Bids Credit:
                        <span class="float-right">-
                            <span id="breakdown_bidsvalue">$0.00</span>
                        </span>
                    </li>

                    <li class="bid_breakdown last">
                        <span id="breakdown_realbids">0</span>
                    Real /
                        <span id="breakdown_voucherbids">0</span>
                    Voucher                            </li>

                    <li>Buy Now Price
                        <span class="float-right breakdown_buynowtotal">$123.99</span>
                    </li>
                </ul>

            </div>
                );
                }
                });

                module.exports =  AuctionInfo;
