'use strict';

var React = require('react/addons');
var Router = require('react-router');
var Link = Router.Link;

var Route = Router.Route;

var Grid = React.createClass({
    processGridItem: function (pos) {
        return React.Children.map(this.props.children, function(child){
            if (child.props.pos === pos) return child;
        });
    },
    render: function () {
        return (
        <div className="Grid">
            <div className="Grid-cell Grid-cell--center u-sizeFull u-posFixed">
                {this.processGridItem("1,1")}
            </div>
            <div className="Grid-cell  Grid-cell--center u-lg-size7of12">
                {this.processGridItem("2,1")}
            </div>
            <div className="Grid-cell u-size1of2 u-lg-size6of12">
                {this.processGridItem("3,1")}
            </div>
            <div className="Grid-cell u-size1of2 u-lg-size4of12">
                {this.processGridItem("3,2")}
            </div>
            <div className="Grid-cell u-size1of3 u-lg-size2of12">
                {this.processGridItem("3,3")}
            </div>
            <div className="Grid-cell u-size1of3">
                {this.processGridItem("4,1")}
            </div>
        </div>
        );
    }
});

module.exports = Grid;
