'use strict';

var React = require('react/addons');
var Router = require('react-router');
var Link = Router.Link;
var AuctionActions = require('../actions/AuctionActions');
var AuctionStore = require('../stores/AuctionStore');
var AuctionTimer = require('./AuctionTimer');
var Reflux = require('reflux');
var Route = Router.Route;

var AuctionGridItem = React.createClass({
    //mixins: [Router.State, Reflux.connect(AuctionStore)],
    mixins: [Reflux.listenTo(AuctionStore,'onSubscribeCompleted')],
    getInitialState(){
        return { liveData: {
            endDate: this.props.endDate,
            auctionPrice: this.props.auctionPrice,
            currentWinner: this.props.currentWinner
        }};
    },
    bid: function(){
        AuctionActions.bid(this.props.id);
    },
    componentDidMount() {
        AuctionActions.subscribe(this.props.id);
    },
    onSubscribeCompleted(liveData){
        if (liveData.id === this.props.id){
            this.setState({'liveData': liveData});
        }
    },
    componentWillUnmount() {
        AuctionActions.unsubscribe(this.props.id);
    },
    render: function () {
        console.log(this.state.liveData);
        return (
            <div className="AuctionGridItem">
                <h5 className="AuctionGridItem-title">
                    <Link to="auction" params={{'aid': this.props.id}}> {this.props.name}</Link>
                </h5>
                <Link to="auction" params={{'aid': this.props.id}}>
                    <img className="AuctionGridItem-image" src={this.props.image}/>
                </Link>
                <h2><AuctionTimer endDate={this.state.liveData.endDate}/></h2>
                <h3>{this.state.liveData.auctionPrice}</h3>
                <h5>{this.state.liveData.currentWinner}</h5>
                <BidButton> </BidButton>
            </div>
        );
    }
});

var BidButton = React.createClass({
    render: function () {
        return (
            <p>
                <a className="Button Button--default" onClick={this.bid}>Bid</a>
            </p>
        );
    }
});

module.exports = AuctionGridItem;
