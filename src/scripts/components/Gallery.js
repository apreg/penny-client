'use strict';
var React = require('react/addons');

var SelectedImg = React.createClass({
    render: function () {
        return (
            <div className="HeroImg">
                <img src={this.props.hero} />
            </div>
        )
    }
});

var ImgItem = React.createClass({
    render: function () {
        return (<img src={this.props.path} />)
    }
});

var ImgRow = React.createClass({
    render: function () {
        return (
            <div className="ImgRow">
                <ul className="ImgRow-list">
            {this.props.children}
                </ul>
            </div>
        )
    }
});

var Gallery = React.createClass({
    handleClick: function (image) {
        this.setState({heroimg: image});
    },
    getInitialState: function () {
        console.log('this.props');
        return {
            heroimg: this.props.images[0],//TODO check if lsit is empty or null
            images: this.props.images
        }
    },
    render: function () {
        console.log('galery');
        return (
            <div className="Gallery">
                <SelectedImg hero={this.state.heroimg} />
                <ImgRow>
                      {this.state.images.map(function (image) {
                          return (
                              <li className="ImgRow-listItem" onClick={this.handleClick.bind(this, image)}>
                                  <ImgItem path={image} />
                              </li>
                          )
                      }, this)}
                </ImgRow>
            </div>
        );
    }
});

module.exports = Gallery;

