'use strict';
var React = require('react/addons');

var AuctionTimer = React.createClass({
    calcRemainingTime (endDate){
        var t = this.props.endDate - new Date().getTime();
        return t > 0 ? t : 0;
    },
    getInitialState: function () {
        return {endDate: this.calcRemainingTime(this.props.endDate)};
    },
    componentDidMount: function () {
        this.timer = setInterval(this.tick, 1000);
    },
    componentWillReceiveProps: function(nextProps) {
        this.setState({
            endDate: this.calcRemainingTime(nextProps.endDate)
        });
    },
    componentWillUnmount: function () {
        clearInterval(this.timer);
    },
    tick: function () {
        var t = this.state.endDate - 1000;
        this.setState({endDate: t > 0 ? t : 0});
    },
    render: function () {
        var time = new Date(this.state.endDate);
        var hours = time.getUTCHours();
        var minutes = time.getUTCMinutes();
        var seconds = time.getUTCSeconds();
        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
        return <span>{(hours > 0 ? pad(hours, 2) + '' : '00') + ':' + (minutes > 0 ? pad(minutes, 2) + '' : '00') + ':' + (seconds > 0 ? pad(seconds, 2) + '' : '00')}</span>;
    }
});

module.exports = AuctionTimer;
