'use strict';

var React = require('react/addons');
var classie = require('classie');
var Router = require('react-router');
var Link = Router.Link;
var AuctionGridItem = require('./AuctionGridItem');

var ListGrid = React.createClass({
    getInitialState: function () {
        return {viewMode: 'gridView'};
    },
    handleSwitch: function (viewMode) {
        this.setState({viewMode: viewMode});
    },
    render: function () {
        var self = this;
        var items = this.props.items.map(function (auction) {
            return (
                <Item viewMode={self.state.viewMode} key={auction.id} id={auction.id}  endDate={auction.endDate} images={auction.item.images} name={auction.item.name} auctionPrice={auction.auctionPrice} timeToBid={auction.timeToBid}/>
            );
        });
        var classString = "ListGrid";
        return (
            <div className={classString}>
                <ViewSwitcher initialView={this.state.viewMode} onSwitch={this.handleSwitch}/>
                <ul className="ViewSwitcher-inner">
                        {items}
                </ul>
            </div>
        );
    }
});

var ViewSwitcher = React.createClass({
    handleClick: function (e) {
        this.props.onSwitch(e.target.getAttribute('data-view'));
    },
    render: function () {
        var c = "ViewSwitcher ViewSwitcher" + "--" + this.props.initialView;
        return (
            <div className={c}>
                <a href="#" className="Icon Icon--grid Icon--large"  onClick={this.handleClick} data-view="gridView">Grid View</a>
                <a href="#" className="Icon Icon--list Icon--large" onClick={this.handleClick} data-view="listView">List View</a>
            </div>
        );
    }
});

var Item = React.createClass({

    render: function () {
        var classString = "Item Item--" + this.props.viewMode;
        return (
            <li className={classString}>
            {/*
                <Link className="Item-image" to="auction" params={{aid: this.props.id}}>
                    <img src={this.props.img}></img>
                </Link>

                <h3 className="Item-title">{this.props.name}</h3>
                <div className="Item-price">{this.props.price}</div>
                <div className="Item-details">
                    {this.props.details}
                </div>
                <a className="cbp-vm-icon cbp-vm-add" href="#">Add to cart</a>
                */}
                <AuctionGridItem  id={this.props.id} endDate={this.props.endDate} name={this.props.name} image={this.props.images[0]} auctionPrice={this.props.auctionPrice} currentWinner={this.props.currentWinner} timeToBid={this.props.timeToBid}></AuctionGridItem>
            </li>
        );
    }

});


module.exports = ListGrid;
