'use strict';

var React = require('react/addons');
var Router = require('react-router');
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var UserStore = require('../../stores/UserStore');
var UserActions = require('../../actions/UserActions');
var Reflux = require('reflux');
var Loader = require('react-loader');

var LoginPage = React.createClass({
    mixins: [Router.State, Router.Navigation, React.addons.LinkedStateMixin, Reflux.listenTo(UserStore, 'onLoginUserCompleted')],

    getInitialState() {
        return {username: '', password: ''};
    },
    login: function (e) {
        e.preventDefault();
        UserActions.loginUser(this.state.username, this.state.password);
    },
    onLoginUserCompleted: function () {
        this.transitionTo(this.getQuery().return_to);
    },
    render: function () {
        return (
            <div className="Page">
                <main className="Container" role="main">
                    <form method="post">
                        <p>
                            <input type="text" name="login" valueLink={this.linkState('username')} placeholder="Username or Email"/>
                        </p>
                        <p>
                            <input type="password" name="password" valueLink={this.linkState('password')} placeholder="Password"/>
                        </p>
                        <p className="remember_me">
                            <label>
                                <input type="checkbox" name="remember_me" id="remember_me"/>
                            Remember me on this computer
                            </label>
                        </p>
                        <p className="submit">
                            <input name="commit" onClick={this.login.bind(this)} type="submit" value="Login"/>
                        </p>
                    </form>
                </main>
            </div>
        );
    }
});

module.exports = LoginPage;

