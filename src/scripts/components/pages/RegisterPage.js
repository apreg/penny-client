'use strict';

var React = require('react/addons');
var ReactTransitionGroup = React.addons.TransitionGroup;
var Header = require('../Header');
var ListGridComponent = require('../List-grid-switcher');
var Gallery = require('../Gallery');
var Grid = require('../Grid');
var GridItem = require('../GridItem');
var Router = require('react-router');
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var AuctionsStore = require('../../stores/AuctionsStore');
var auctionActions = require('../../actions/AuctionActions');
var Reflux = require('reflux');
var Loader = require('react-loader');

var RegisterPage = React.createClass({
    mixins: [Router.State, Reflux.connect(AuctionsStore)],

    render: function () {
        return (
            <div className="Page">
                <main className="Container" role="main">

                </main>
            </div>
        );
    }
});

module.exports = RegisterPage;

