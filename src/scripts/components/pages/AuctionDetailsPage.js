'use strict';
var Header = require('../Header');
var React = require('react/addons');
var Router = require('react-router');
var Gallery = require('../Gallery');
var AuctionGridItem = require('../AuctionGridItem');
var Link = Router.Link;
var Route = Router.Route;
var AuctionStore = require('../../stores/AuctionStore');
var auctionActions = require('../../actions/AuctionActions');
var Reflux = require('reflux');
var Loader = require('react-loader');

var AuctionDetailsPage = React.createClass({
    mixins: [Router.State, Reflux.connect(AuctionStore)],

    componentDidMount() {
        auctionActions.getAuction(this.context.getCurrentParams().aid);
    },

    render: function () {
        return (
                <Loader loaded={this.state.loaded}>
                    <div className="Page">
                        <main className="Container u-sm-size4of4 u-md-size4of4 u-lg-size3of5" role="main">
                            <div className="Grid">
                                <div className="Grid-cell Grid-cell--center u-sizeFull">
                                    <div className="wrapComponent">
                                        <h3 className="AuctionDetails-title">{this.state.auction.item.name}</h3>
                                    </div>
                                </div>
                                <div className="Grid-cell u-size1of3 ">
                                    <Gallery images={this.state.auction.item.images} />
                                </div>
                                <div className="Grid-cell u-size1of3">
                                    <AuctionGridItem id={this.state.auction.id} endDate={this.state.auction.endDate} name={this.state.auction.item.name} image={this.state.auction.item.images[0]} auctionPrice={this.state.auction.auctionPrice} currentWinner={this.state.auction.currentWinner}></AuctionGridItem>
                                </div>
                                <div className="Grid-cell u-size1of3">
                                history
                                </div>
                            </div>
                        </main>
                    </div>
                </Loader>
        );
    }
});

module.exports = AuctionDetailsPage;

