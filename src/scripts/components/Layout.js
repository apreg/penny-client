'use strict';

var React = require('react/addons');
var ReactTransitionGroup = React.addons.TransitionGroup;
var Header = require('./Header');
var ListGridComponent = require('./List-grid-switcher');
var Router = require('react-router');
var DefaultRoute = Router.DefaultRoute;

var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

// Export React so the devtools can find it
(window !== window.top ? window.top : window).React = React;

var Layout = React.createClass({
    render: function () {
        return (
            <div>
                <Header>
                </Header>
                <RouteHandler/>
            </div>
        );
    }
});

module.exports = Layout;
