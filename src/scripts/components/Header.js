'use strict';

var React = require('react/addons');
var Router = require('react-router');
var UserStore = require('../stores/UserStore');
var UserActions = require('../actions/UserActions');
var Link = Router.Link;
var Reflux = require('reflux');

var Route = Router.Route;

var Header = React.createClass({
    mixins: [Router.State, Router.Navigation, Reflux.connect(UserStore), Reflux.listenTo(UserStore, 'onLogoutUser')],
    handleLogout: function (e) {
        e.preventDefault();
        UserActions.logoutUser();
    },
    onLogoutUser: function () {
        this.transitionTo('/');
    },
    render: function () {
        return (
            <header className="Header u-posFixed u-sizeFull" role="banner">
            { UserStore.isLoggedIn() ?
                <div className="Header-inner u-sm-size4of4 u-md-size4of4 u-lg-size3of5 u-cf">
                    <h1 className="Header-logo u-floatLeft">
                        <Link to="auction" className="Icon Icon--auction"></Link>
                    </h1>
                    //<a href="#" className={"icon icon-left-nav pull-left" + (this.props.back === "true" ? "" : " hidden")}></a>
                    <div className="Header-right u-floatRight">
                        <Link to="/" className="Button Button--default" onClick={this.handleLogout}>Logout</Link>
                    </div>
                </div>
                :
                <div className="Header-inner u-sm-size4of4 u-md-size4of4 u-lg-size3of5 u-cf">
                    <h1 className="Header-logo u-floatLeft">
                        <Link to="auction" className="Icon Icon--auction"></Link>
                    </h1>
                    //<a href="#" className={"icon icon-left-nav pull-left" + (this.props.back === "true" ? "" : " hidden")}></a>
                    <div className="Header-right u-floatRight">
                        <Link to="login" className="Button Button--default" query={{return_to: this.getPath()}}>Login</Link>
                        <Link to="register" className="Button Button--default">Register</Link>
                    </div>
                </div>}

            </header>
        );
    }
});

module.exports = Header;

