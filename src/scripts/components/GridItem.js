'use strict';

var React = require('react/addons');
var Router = require('react-router');

var Link = Router.Link;

var Route = Router.Route;

var GridItem = React.createClass({
    render: function () {
        return (
            this.props.children
        );
    }
});

module.exports = GridItem;
